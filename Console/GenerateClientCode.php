<?php
namespace Ows\Grpc\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

/**
 * Class GenerateClientCode
 * @package Ows\Grpc\Console
 */
class GenerateClientCode extends Command
{
    /**
     * Configure command
     */
    protected function configure()
    {
        $this->setName('gprc:generate-client:code');
        $this->addOption('name', null, InputOption::VALUE_REQUIRED, 'The path to the generate code file');
        $this->setDescription('Generate Client Code');

        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($fileName = $input->getOption('name')) {
            $process = new Process('./app/code/Ows/Grpc/etc/' . $fileName);
            $process->run();

            if (!$process->isSuccessful()) {
                throw new ProcessFailedException($process);
            }

            $output->writeln($process->getOutput());
            $output->writeln('Success!');
        } else {
            $output->writeln('<error>The option [--path] is require.</error>');
        }
    }
}