<?php
namespace Ows\Grpc\Model;

use Grpc\ChannelCredentials;
use Helloworld\GreeterClient;
use Helloworld\HelloRequest;
use Magento\Tests\NamingConvention\true\string;

@include_once dirname(__FILE__) . '/Helloworld/GreeterClient.php';
@include_once dirname(__FILE__) . '/Helloworld/HelloReply.php';
@include_once dirname(__FILE__) . '/Helloworld/HelloRequest.php';
@include_once dirname(__FILE__) . '/GPBMetadata/Helloworld.php';

/**
 * Class Helloworld
 * @package Ows\Grpc\Model
 */
class Helloworld
{

    /**
     * Greet Function
     * @param string $name
     *
     * @return mixed
     */
    function greet($name)
    {
        $client  = new GreeterClient(
            'localhost:50051',
            [
                'credentials' => ChannelCredentials::createInsecure(),
            ]
        );
        $request = new HelloRequest();
        $request->setName($name);
        [$reply, $status] = $client->SayHello($request)->wait();
        $message = $reply->getMessage();

        return $message;
    }
}