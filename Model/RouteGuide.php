<?php

namespace Ows\Grpc\Model;

use Grpc\ChannelCredentials;
use Routeguide\RouteGuideClient;

include_once dirname(__FILE__) . '/Routeguide/Feature.php';
include_once dirname(__FILE__) . '/Routeguide/Point.php';
include_once dirname(__FILE__) . '/Routeguide/Rectangle.php';
include_once dirname(__FILE__) . '/Routeguide/RouteGuideClient.php';
include_once dirname(__FILE__) . '/Routeguide/RouteNote.php';
include_once dirname(__FILE__) . '/Routeguide/RouteSummary.php';
include_once dirname(__FILE__) . '/GPBMetadata/RouteGuide.php';

define('COORD_FACTOR', 1e7);

/**
 * Class RouteGuide
 * @package Ows\Grpc\Model
 */
class RouteGuide
{

    /**
     * @var RouteGuideClient
     */
    protected $client;

    /**
     * Set client value
     */
    public function execute()
    {
        $this->client = new RouteGuideClient(
            'localhost:50051',
            [
                'credentials' => ChannelCredentials::createInsecure(),
            ]
        );
    }

    /**
     * Run the getFeature demo. Calls getFeature with a point known to have a
     * feature and a point known not to have a feature.
     */
    function runGetFeature()
    {
        echo "Running GetFeature...\n";

        $point  = new \Routeguide\Point();
        $points = [
            [409146138, -746188906],
            [0, 0],
        ];

        foreach ($points as $p) {
            $point->setLatitude($p[0]);
            $point->setLongitude($p[1]);
            // make a unary grpc call
            [$feature, $status] = $this->client->GetFeature($point)->wait();
            $this->printFeature($feature);
        }
    }

    /**
     * @param $feature
     */
    function printFeature($feature)
    {
        $name = $feature->getName();
        if (!$name) {
            $name_str = 'no feature';
        } else {
            $name_str = "feature called $name";
        }
        echo sprintf(
            "Found %s \n  at %f, %f\n",
            $name_str,
            $feature->getLocation()->getLatitude() / COORD_FACTOR,
            $feature->getLocation()->getLongitude() / COORD_FACTOR
        );
    }

    /**
     * Run the listFeatures demo. Calls listFeatures with a rectangle
     * containing all of the features in the pre-generated
     * database. Prints each response as it comes in.
     */
    function runListFeatures()
    {
        echo "Running ListFeatures...\n";

        $lo_point = new \Routeguide\Point();
        $hi_point = new \Routeguide\Point();

        $lo_point->setLatitude(400000000);
        $lo_point->setLongitude(-750000000);
        $hi_point->setLatitude(420000000);
        $hi_point->setLongitude(-730000000);

        $rectangle = new \Routeguide\Rectangle();
        $rectangle->setLo($lo_point);
        $rectangle->setHi($hi_point);

        // start the server streaming call
        $call = $this->client->ListFeatures($rectangle);
        // an iterator over the server streaming responses
        $features = $call->responses();
        foreach ($features as $feature) {
            $this->printFeature($feature);
        }
    }

    /**
     * Run the recordRoute demo. Sends several randomly chosen points from the
     * pre-generated feature database with a variable delay in between. Prints
     * the statistics when they are sent from the server.
     */
    function runRecordRoute()
    {
        echo "Running RecordRoute...\n";
        global $argv;

        // start the client streaming call
        $call = $this->client->RecordRoute();

        $db               = json_decode(file_get_contents($argv[1]), true);
        $num_points_in_db = count($db);
        $num_points       = 10;
        for ($i = 0; $i < $num_points; ++$i) {
            $point        = new \Routeguide\Point();
            $index        = rand(0, $num_points_in_db - 1);
            $lat          = $db[$index]['location']['latitude'];
            $long         = $db[$index]['location']['longitude'];
            $feature_name = $db[$index]['name'];
            $point->setLatitude($lat);
            $point->setLongitude($long);
            echo sprintf(
                "Visiting point %f, %f,\n  with feature name: %s\n",
                $lat / COORD_FACTOR,
                $long / COORD_FACTOR,
                $feature_name ? $feature_name : '<empty>'
            );
            usleep(rand(300000, 800000));
            $call->write($point);
        }
        [$route_summary, $status] = $call->wait();
        echo sprintf(
            "Finished trip with %d points\nPassed %d features\n" .
            "Travelled %d meters\nIt took %d seconds\n",
            $route_summary->getPointCount(),
            $route_summary->getFeatureCount(),
            $route_summary->getDistance(),
            $route_summary->getElapsedTime()
        );
    }

    /**
     * Run the routeChat demo. Send some chat messages, and print any chat
     * messages that are sent from the server.
     */
    function runRouteChat()
    {
        echo "Running RouteChat...\n";

        // start the bidirectional streaming call
        $call = $this->client->RouteChat();

        $notes = [
            [1, 1, 'first message'],
            [1, 2, 'second message'],
            [2, 1, 'third message'],
            [1, 1, 'fourth message'],
            [1, 1, 'fifth message'],
        ];

        foreach ($notes as $n) {
            $point = new \Routeguide\Point();
            $point->setLatitude($lat = $n[0]);
            $point->setLongitude($long = $n[1]);

            $route_note = new \Routeguide\RouteNote();
            $route_note->setLocation($point);
            $route_note->setMessage($message = $n[2]);

            echo sprintf(
                "Sending message: '%s' at (%d, %d)\n",
                $message,
                $lat,
                $long
            );
            // send a bunch of messages to the server
            $call->write($route_note);
        }
        $call->writesDone();

        // read from the server until there's no more
        while ($route_note_reply = $call->read()) {
            echo sprintf(
                "Previous left message at (%d, %d): '%s'\n",
                $route_note_reply->getLocation()->getLatitude(),
                $route_note_reply->getLocation()->getLongitude(),
                $route_note_reply->getMessage()
            );
        }
    }
}